#!/usr/bin/env bash

rpm -e --nodeps kernel-devel* kernel-headers*
rpm -ivh --force http://mirror.nexcess.net/openvz/kernel/branches/rhel6-$(uname -r | tr - /)/vzkernel-devel-$(uname -r).$(uname -m).rpm
rpm -ivh --force http://mirror.nexcess.net/openvz/kernel/branches/rhel6-$(uname -r | tr - /)/vzkernel-headers-$(uname -r).$(uname -m).rpm
yum -y install gcc make perl
mkdir /tmp/virtualbox
VERSION=$(cat /home/vagrant/.vbox_version)
mount -o loop /home/vagrant/VBoxGuestAdditions_$VERSION.iso /tmp/virtualbox
sh /tmp/virtualbox/VBoxLinuxAdditions.run
umount /tmp/virtualbox
rmdir /tmp/virtualbox
rm /home/vagrant/*.iso
